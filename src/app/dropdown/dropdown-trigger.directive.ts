import { Directive, Input, HostListener, ViewContainerRef, OnDestroy, ElementRef } from '@angular/core';
import {
  Overlay,
  OverlayRef,
  ConnectedPositionStrategy,
  FlexibleConnectedPositionStrategy
} from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { merge, Subscription } from 'rxjs';
import { DropdownComponent } from './dropdown.component';

@Directive({
  selector: '[appDropdownTrigger]',
  exportAs: 'appDropdownTrigger'
})
export class DropdownTriggerDirective implements OnDestroy {
  private _dropdown: DropdownComponent;
  private overlayRef: OverlayRef;
  private portal: TemplatePortal;
  private closeSubscription: Subscription;

  @Input('appDropdownTrigger') set dropdown(dropdown: DropdownComponent) {
    if (this.dropdown !== dropdown) {
      this._dropdown = dropdown;
    }
  }

  @HostListener('click') handleClick() {
    this.open();
  }

  constructor(private overlay: Overlay, private viewContainerRef: ViewContainerRef, private elementRef: ElementRef) {}

  ngOnDestroy() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
    }

    this.closeSubscription.unsubscribe();
  }

  open() {
    this.overlayRef = this.createOverlay();
    this.portal = this.getPortal();
    this.overlayRef.attach(this.portal);
    this.closeSubscription = this.closingActions().subscribe(() => this.close());
  }

  close() {
    if (this.overlayRef) {
      this.overlayRef.detach();
    }
  }

  private createOverlay() {
    return this.overlay.create({
      hasBackdrop: true,
      backdropClass: 'cdk-overlay-transparent-backdrop',
      positionStrategy: this.overlay
        .position()
        .connectedTo(this.elementRef, { originX: 'start', originY: 'bottom' }, { overlayX: 'start', overlayY: 'top' })
    });
  }

  private closingActions() {
    const backdrop = this.overlayRef.backdropClick();
    const dropdownClose = this._dropdown.close$.asObservable();

    return merge(backdrop, dropdownClose);
  }

  private getPortal() {
    return new TemplatePortal(this._dropdown.templateRef, this.viewContainerRef);
  }
}
