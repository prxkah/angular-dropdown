import { Component, ViewChild, TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-dropdown',
  template: `
    <ng-template>
      <ng-content></ng-content>
    </ng-template>
  `,
  exportAs: 'appDropdown'
})
export class DropdownComponent {
  close$ = new Subject();

  @ViewChild(TemplateRef) templateRef: TemplateRef<any>;

  close() {
    this.close$.next();
  }
}
