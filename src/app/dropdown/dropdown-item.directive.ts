import { Directive, Host, HostListener } from '@angular/core';
import { DropdownComponent } from './dropdown.component';

@Directive({
  selector: '[appDropdownItem]'
})
export class DropdownItemDirective {
  constructor(@Host() private dropdownComponent: DropdownComponent) {}

  @HostListener('click') handleClick() {
    this.dropdownComponent.close();
  }
}
