import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { DropdownComponent } from './dropdown.component';
import { DropdownTriggerDirective } from './dropdown-trigger.directive';
import { DropdownItemDirective } from './dropdown-item.directive';

const components = [DropdownComponent, DropdownTriggerDirective, DropdownItemDirective];

@NgModule({
  declarations: [...components],
  imports: [CommonModule, OverlayModule],
  exports: [...components]
})
export class DropdownModule {}
