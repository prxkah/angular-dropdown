import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <button class="btn btn-secondary" [appDropdownTrigger]="menu">Open</button>
    <app-dropdown #menu="appDropdown">
      <div class="dropdown">
        <div class="dropdown-menu show">
          <a appDropdownItem class="dropdown-item">One</a>
          <a appDropdownItem class="dropdown-item">Two</a>
        </div>
      </div>
    </app-dropdown>
  `
})
export class HomeComponent {
  alert() {
    alert('close');
  }
}
