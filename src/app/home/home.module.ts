import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { DropdownModule } from '../dropdown/dropdown.module';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: HomeComponent}
]

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, DropdownModule, RouterModule.forChild(routes)],
  exports: [],
  providers: []
})
export class HomeModule {}
